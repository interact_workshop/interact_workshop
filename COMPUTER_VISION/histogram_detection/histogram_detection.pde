
import hypermedia.video.*;
import integralhistogram.*;

OpenCV opencv;
PImage temp;
PImage imgTarget;

TargetUser targetImg;

//Set the size of the color histograms by defining the number of bit
//par components(RGB) so bit = 2 means 6 bit per pixel i.e 2^6 possible
// different RGB colors
int bit = 2;
int sw = 320, sh = 240;

void setup() 
{
  size(sw,120);

  opencv = new OpenCV(this);
  opencv.capture(sw, sh);
  temp = createImage(sw, sh, RGB);
  imgTarget   = loadImage("plectrum.jpg");

  // 0.15 is tolerance i.e = "0.15 is the max error you can do" between [0,1]
  // 10 is scale i.e 10 = "start searching at 1/10 of the target image size"
  targetImg = new MyTargetUser(imgTarget,bit,10,0.1);
  targetImg.setOutputImage(opencv.image());
}

void draw()
{
  background(255);
  opencv.read();
  temp = opencv.image();
  // create and use the integral histogram
  IntHistUser test = new MyIntHistUser(temp, bit);

//  image(opencv.image(), 0, 0, 160, 120); // Draw the webcam video onto the screen

//  maskResult();

  if(test.imageSearch(targetImg)) image(imgTarget,160, 0, 160, 120);
image(temp, 0, 0, sw/2, sh/2);
//  opencv.image().updatePixels();
}

void maskResult()
{
  for(int i=0;i<opencv.image().width*opencv.image().height;i++)
    opencv.image().pixels[i] = 0|0|(opencv.image().pixels[i])&0xFF;
}       

