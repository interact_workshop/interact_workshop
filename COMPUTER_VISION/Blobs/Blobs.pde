import hypermedia.video.*;
import java.awt.Rectangle;

OpenCV opencv;
int sw = 320, sh = 240;
int threshold = 80;

void setup() {

  size( sw, sh );

  // open video stream
  opencv = new OpenCV( this );
  opencv.capture( sw, sh );
}

void draw() {

  background(192);

  opencv.read();           // grab frame from camera
  opencv.threshold(threshold);    // set black & white threshold 

    // find blobs
  Blob[] blobs = opencv.blobs( 10, width*height/2, 100, true, OpenCV.MAX_VERTICES*4 );

  // draw blob results
  for( int i=0; i<blobs.length; i++ ) {
    
    fill(255, 0, 0, 125);
    Rectangle r = blobs[i].rectangle;
    rect(r.x, r.y, r.width, r.height);
    
    beginShape();
    for( int j=0; j<blobs[i].points.length; j++ ) {
      vertex( blobs[i].points[j].x, blobs[i].points[j].y );
    }
    endShape(CLOSE);

    
  }

  if(threshold > 255) threshold = 255;
  if(threshold < 0) threshold = 0;
  println(threshold);
}

void keyPressed() {
  // change threshold with the keyboard
  if(key == 'p') threshold++;
  if(key == 'm') threshold--;
}

