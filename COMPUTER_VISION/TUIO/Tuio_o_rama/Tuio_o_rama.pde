/*
You need Reactivision Framework to use this sketch

*/

import TUIO.*;
TuioProcessing tuioClient;

float object_size = 60;
float table_size = 760;
float scale_factor = 1;
PFont font;
int indexAdd=0, nbImages = 3, count = 0;
ArrayList <PImage> visages = new ArrayList <PImage>();
PImage[] mesImagesOn = new PImage[nbImages];
PImage[] mesImagesOff = new PImage[nbImages];  

void setup()
{
  size(320,240);
  noStroke();
  fill(0);

  loop();
  frameRate(30);

  hint(ENABLE_NATIVE_FONTS);
  font = createFont("Arial", 18);
  scale_factor = height/table_size;

  tuioClient  = new TuioProcessing(this);

  for(int i=0; i<nbImages; i++) {
    mesImagesOn[i] = loadImage("im"+i+".jpg");
    mesImagesOff[i] = loadImage("imOff"+i+".jpg");
    mesImagesOn[i].mask(mesImagesOff[i]);
    visages.add(mesImagesOn[i]);
  }
}

void draw()
{
  background(255);
  textFont(font,18*scale_factor);
  float obj_size = object_size*scale_factor; 

  Vector tuioObjectList = tuioClient.getTuioObjects();
  for (int i=0;i<tuioObjectList.size();i++) {
    TuioObject tobj = (TuioObject)tuioObjectList.elementAt(i);
    if(i<visages.size()) {
      PImage  visage = (PImage) visages.get(i);
      pushMatrix();
      translate(tobj.getScreenX(width),tobj.getScreenY(height));
      rotate(tobj.getAngle());
      image(visage,-obj_size/2,-obj_size/2,obj_size,obj_size);
      popMatrix();
    }
    else {
      PImage  visage = (PImage) visages.get(i-visages.size());
      pushMatrix();
      translate(tobj.getScreenX(width),tobj.getScreenY(height));
      rotate(tobj.getAngle());
      image(visage,-obj_size/2,-obj_size/2,obj_size,obj_size);
      popMatrix();
    }


    fill(255);
    text(""+tobj.getSymbolID(), tobj.getScreenX(width), tobj.getScreenY(height));
  }
}

void addTuioObject(TuioObject tobj) {
//  println("add object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle());
}

void removeTuioObject(TuioObject tobj) {
//  println("remove object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+")");
}

void updateTuioObject (TuioObject tobj) {
//  println("update object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle()
//    +" "+tobj.getMotionSpeed()+" "+tobj.getRotationSpeed()+" "+tobj.getMotionAccel()+" "+tobj.getRotationAccel());
}

// called after each message bundle
// representing the end of an image frame
void refresh(TuioTime bundleTime) { 
  redraw();
}

