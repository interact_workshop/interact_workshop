import hypermedia.video.*;

OpenCV opencv;

void setup() {
  size( 320, 240 );
  // open video stream
  opencv = new OpenCV( this );
  opencv.capture( 160, 120 );
}

void draw() {
  opencv.read();                               // grab frame from camera
  image( opencv.image(), 0, 0);                // show the original image

  opencv.absDiff();                            // make the difference between the current image and the image in memory

  image( opencv.image(OpenCV.MEMORY), 160, 0 );  // display the image in memory on the right
  image( opencv.image(), 160, 120 );             // display the result on the bottom right
}

void keyPressed() {
  opencv.remember();  // store the actual image in memory
}

