import hypermedia.video.*;

OpenCV opencv;
PImage livecamOpen;

void setup() {
  size(320, 240); // Change size to 320 x 240 if too slow at 640 x 480
  opencv = new OpenCV(this);
  opencv.capture(width,height);
  livecamOpen = new PImage(width, height);
  noStroke();
  smooth();
}

void draw() {

  image(livecamOpen, 0, 0, width, height); // Draw the webcam video onto the screen
  opencv.read();
  int[] live = opencv.image().pixels;//----------------------récupère les valeurs des pixels de l'image actuelle
  int brightestX = 0; // X-coordinate of the brightest video pixel
  int brightestY = 0; // Y-coordinate of the brightest video pixel
  float brightestValue = 0; // Brightness of the brightest video pixel

  livecamOpen.loadPixels();
  int index = 0;
  for (int x = 0; x < width; x++) {
    for (int y = 0; y < height; y++) {
      int loc = x + y *width;
      livecamOpen.pixels[loc]=live[loc];//------------------------bascule les pixels de live dans liveCamOpen

      float pixelBrightness = brightness(live[loc]);
      if (pixelBrightness > brightestValue) {
        brightestValue = pixelBrightness;
        brightestY = y;
        brightestX = x;
      }
    }
  }
  livecamOpen.updatePixels();
  fill(0, 255, 128);
  ellipse(brightestX, brightestY, 20, 20);
}


