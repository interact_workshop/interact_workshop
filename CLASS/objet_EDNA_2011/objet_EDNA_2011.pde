Pomme golden;

void setup() {
  smooth();
  golden = new Pomme();
  golden.r = 156;
  golden.g = 200;
  golden.b = 30;
  golden.taille = 20;
  golden.x = width/2;
  golden.y = height/2;
}

void draw() {
  background(255);
  golden.dessine();
  golden.grossir();
  golden.fall();
}

class Pomme {
  int r, g, b;
  float taille;
  float x, y;
  int mure = 60;


  void grossir() {
    taille += 0.3;
  }

  void fall() {
    if(taille > mure) {
      taille = mure;
      y++;
    }
    if(y > height - taille/2) { 
      y =  height - taille/2;
      println("pourri");
      pourrir();
    }
  }

  void pourrir() {
    r--;g--;b--;mure--;
  }

  void dessine() {
    fill(r, g, b);
    ellipse(x, y, taille, taille);
  }
}

