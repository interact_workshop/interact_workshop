/*

 Tracking for the brightest pixel in the preview camera.
 This sketch come with a Ketai.jar modified, to use with Samsung Galaxy Spica i5700.
 You need to activate this permissions:
 
 CAMERA
 
 */

import edu.uic.ketai.inputService.KetaiCamera;

KetaiCamera cam;

/************************************************************************

--------------------------------  SETUP ---------------------------------

*************************************************************************/

void setup() {
  //reduce frameRate because Spica is a cheap CPU 800MHz  
  frameRate(15);
  orientation(LANDSCAPE);
  cam = new KetaiCamera(this, 160, 90, 15);
  noStroke();
}

/************************************************************************

--------------------------------  DRAW ---------------------------------

*************************************************************************/

void draw() {
  image(cam, 0, 0, screenWidth, screenHeight);
 
  int brightestX = 0; // X-coordinate of the brightest video pixel
  int brightestY = 0; // Y-coordinate of the brightest video pixel
  float brightestValue = 0; // Brightness of the brightest video pixel

  int index = 0;
  for (int y = 0; y < cam.frameHeight; y++) {
    for (int x = 0; x < cam.frameWidth; x++) {
      // Get the color stored in the pixel
      int pixelValue = cam.pixels[index];
      // Determine the brightness of the pixel
      float pixelBrightness = brightness(pixelValue);
      // If that value is brighter than any previous, then store the
      // brightness of that pixel, as well as its (x,y) location
      if (pixelBrightness > brightestValue) {
        brightestValue = pixelBrightness;
        brightestY = y;
        brightestX = x;
      }
      index++;
    }
  }
  int x = (int)map(brightestX, 0, cam.frameWidth, 0, screenWidth);
  int y = (int)map(brightestY, 0, cam.frameHeight, 0, screenHeight);
  fill(255, 204, 0, 128);
  ellipse(x, y, 50, 50);
}

/************************************************************************

--------------------------------  EVENTS ---------------------------------

*************************************************************************/

void onCameraPreviewEvent()
{
  cam.read();
}

void exit() {
  cam.stop();
}

// start/stop camera preview by tapping the screen
void mousePressed()
{
  if (cam.isStarted())
  {
    cam.stop();
  }
  else
    cam.start();
}

