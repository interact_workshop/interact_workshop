/*

 For each face detection, the phone vibrate and make a sound.
 This sketch come with a Ketai.jar modified, to use with Samsung Galaxy Spica i5700.
 You need to activate this permissions:
 
 CAMERA
 WRITE_EXTERNAL_STORAGE
 
 You need to create this folder if you to save a pic : /sd_card/ketai_data/ et desactiver l'usb
 When you take a color picture you need to touch the screen to stop and reactivate the camera stream
 */
import edu.uic.ketai.inputService.KetaiCamera;

KetaiCamera cam;
/************************************************************************

--------------------------------  SETUP ---------------------------------

*************************************************************************/

void setup() {
  orientation(LANDSCAPE);
  cam = new KetaiCamera(this, 240, 160, 24);
}

/************************************************************************

--------------------------------  DRAW ---------------------------------

*************************************************************************/
void draw() {
  image(cam, 0, 0, screenWidth, screenHeight);
}

void onCameraPreviewEvent()
{
  cam.read();
}

void exit() {
  cam.stop();
}

// start/stop camera preview by tapping the screen
void mousePressed()
{
  if (cam.isStarted())
  {
    cam.stop();
  }
  else
    cam.start();
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == DPAD) {
      println("NB photo!");
      // en NB et basse définition
      save("/sdcard/screen.png");
    }
    else if (keyCode == DOWN) {
      //en couleur et haute définition
      println("color picture !");
      cam.takePicture();
      redraw();
    }
  }
}

