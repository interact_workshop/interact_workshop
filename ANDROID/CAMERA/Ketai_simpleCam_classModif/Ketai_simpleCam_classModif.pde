/*
YCbCr est une manière de représenter l'espace colorimétrique en vidéo,
 issue essentiellement des problèmes de transmission hertzienne.
 Une image captée par n'importe quel appareil est la somme des couleurs qui la composent,
 que le résultat soit en couleur ou en noir et blanc. Ainsi, même une image en noir et blanc,
 le signal Y en l'occurrence a été créé par la somme du rouge, du bleu et du vert.
 On envoie Y, c'est-à-dire l'information de luminance ou noir et blanc plus 
 deux informations de chrominance, Cb et Cr respectivement le bleu moins Y et le rouge moins Y.
 Le récepteur peut recréer le vert et reproduire une image couleur, en effet si on a Y (rouge+vert+bleu)
 et Cb (Y-bleu) et Cr (Y-rouge) on peut mathématiquement recréer le vert en utilisant l'équation:
 Y = 0,3R + 0,6V + 0,1B
 
 L'ajout de 128 à Cb et Cr permet d'obtenir des valeurs entre 0 et 255 
 (en langage C, il s'agit du type unsigned char) La conversion inverse se fait ainsi 
 (les valeurs obtenues varient encore entre 0 et 255) :
 
 R = Y + 1,402 * (Cr − 128)
 G = Y − 0,34414 * (Cb − 128) − 0,71414 * (Cr − 128)
 B = Y + 1,772 * (Cb − 128)
 
 */

import edu.uic.ketai.inputService.KetaiCamera;
MKetaiCamera cam;

void setup() {
  orientation(LANDSCAPE);
  cam = new MKetaiCamera(this, 240, 160, 15);
}

void draw() {
  image(cam, 0, 0, screenWidth, screenHeight);

  if (keyPressed) {
    if (key == CODED) {
      if (keyCode == DPAD) {
        cam.PRINT_RGB = true;
      }
    }
  }
  else {
    cam.PRINT_RGB = false;
  }
}

void onCameraPreviewEvent()
{
  cam.read();
}

void exit() {
  cam.stop();
}

// start/stop camera preview by tapping the screen
void mousePressed()
{
  if (cam.isStarted())
  {
    cam.stop();
  }
  else
    cam.start();
}

