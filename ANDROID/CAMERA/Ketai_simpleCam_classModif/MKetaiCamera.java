import processing.core.*;

import edu.uic.ketai.inputService.KetaiCamera;
import edu.uic.ketai.analyzer.IKetaiAnalyzer;
import edu.uic.ketai.inputService.IKetaiInputService;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
//import android.graphics.ImageFormat;

class MKetaiCamera extends KetaiCamera {

  private ArrayList<IKetaiAnalyzer> listeners;
  private PApplet parent;
  private Camera camera;
  public boolean isStarted;
  private int[] myPixels;
  public int frameWidth, frameHeight, cameraFPS;
  private Method onPreviewEventMethod, onPreviewEventMethodPImage;
  PImage self;
  Thread runner;
  boolean available = false;
  static boolean PRINT_RGB = false;

  MKetaiCamera(PApplet pParent, int _width, int _height, int _framesPerSecond) {
    super(pParent, _width, _height, _framesPerSecond);
    parent = pParent;
    frameWidth = _width;
    frameHeight = _height;
    cameraFPS = _framesPerSecond;
    isStarted = false;
    super.init(_width, _height, RGB);
    myPixels = new int[_width * _height];
    listeners = new ArrayList<IKetaiAnalyzer>();
    self = this;

    try {
      // the following uses reflection to see if the parent
      // exposes the call-back method. The first argument is the method
      // name followed by what should match the method argument(s)
      onPreviewEventMethod = parent.getClass().getMethod(
      "onCameraPreviewEvent");
      PApplet.println("KetaiCamera found onCameraPreviewEvent in parent... ");
    } 
    catch (Exception e) {
      // no such method, or an error.. which is fine, just ignore
      onPreviewEventMethod = null;
      PApplet.println("KetaiCamera did not find onCameraPreviewEvent Method: "
        + e.getMessage());
    }

    try {
      // the following uses reflection to see if the parent
      // exposes the call-back method. The first argument is the method
      // name followed by what should match the method argument(s)
      onPreviewEventMethodPImage = parent.getClass().getMethod(
      "onCameraPreviewEvent", new Class[] { 
        KetaiCamera.class
      }
      );
      PApplet.println("KetaiCamera found onCameraPreviewEvent for PImage in parent... ");
    } 
    catch (Exception e) {
      // no such method, or an error.. which is fine, just ignore
      onPreviewEventMethodPImage = null;
      PApplet.println("KetaiCamera did not find onCameraPreviewEvent for Image Method: "
        + e.getMessage());
    }

    PApplet.println("KetaiCamera completed instantiation... ");
    // runner = new Thread(this);
    // runner.run();
  }


  @Override
    public void start() {
    PApplet.println("KetaiCamera: opening camera...");
    if (camera == null)
      camera = Camera.open();
    camera.setPreviewCallback(previewcallback);

    Parameters cameraParameters = camera.getParameters();
    cameraParameters.setPreviewFrameRate(cameraFPS);
    cameraParameters.setPreviewSize(frameWidth, frameHeight);
    // cameraParameters.setPreviewFormat(ImageFormat.RGB_565);
    //cameraParameters.setPictureFormat(ImageFormat.RGB_565);
    camera.setParameters(cameraParameters);
    isStarted = true;
    PApplet.println("KetaiCamera: Set camera parameters...");
    //camera.setPreviewCallback(previewcallback);
    camera.startPreview();
  }

  public void takePicture() {
    if (camera != null)
      camera.takePicture(null, null, jpegCallback);
  }

  public void onResume() {
    // if (camera == null) {
    // camera = Camera.open();
    // camera.startPreview();
    // }
  }
  PictureCallback rawCallback = new PictureCallback() { // <7>
    public void onPictureTaken(byte[] data, Camera camera) {
    }
  };

  public void read() {
    loadPixels();
    synchronized (pixels) {
      System.arraycopy(myPixels, 0, pixels, 0, frameWidth * frameHeight);
      available = false;
      updatePixels();
    }
  }

  public boolean isStarted() {
    return isStarted;
  }



  //@Override
  PreviewCallback previewcallback = new PreviewCallback() {
    public void onPreviewFrame(byte[] data, Camera camera) {

      if (camera == null || !isStarted)
        return;

      if (myPixels == null)
        myPixels = new int[frameWidth * frameHeight];

      // camera.getParameters().getPreviewSize().width;
      MKetaiCamera.decodeYUV420SP(myPixels, data, frameWidth, frameHeight);

      if (myPixels == null)
        return;

      // PApplet.println("KetaiCamera.previewCallback: pixels buffer is of length: "
      // + myPixels.length +"/"+onPreviewEventMethod);
      if (onPreviewEventMethod != null && myPixels != null)
      try {
        onPreviewEventMethod.invoke(parent);
      } 
      catch (Exception e) {
        PApplet.println("Disabling onCameraPreviewEvent() because of an error:"
          + e.getMessage());
        e.printStackTrace();
        onPreviewEventMethod = null;
      }

      if (onPreviewEventMethodPImage != null && myPixels != null)
      try {
        onPreviewEventMethodPImage.invoke(parent, 
        new Object[] { 
          (MKetaiCamera) self
        }
        );
      } 
      catch (Exception e) {
        PApplet.println("Disabling onCameraPreviewEvent(KetaiCamera) because of an error:"
          + e.getMessage());
        e.printStackTrace();
        onPreviewEventMethodPImage = null;
      }
      broadcastData(self);
    }
  };

  PictureCallback jpegCallback = new PictureCallback() {
    public void onPictureTaken(byte[] data, Camera camera) {
      if (camera == null)
        return;
      FileOutputStream outStream = null;
      // BitmapFactory.Options options = new BitmapFactory.Options();
      // options.inSampleSize = 1;
      // Bitmap bitmap = BitmapFactory
      // .decodeByteArray(data, 0, data.length, options);
      // int w = bitmap.getWidth();
      // int h = bitmap.getHeight();
      // int[] pixels = new int[w * h];
      // bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
      //
      // Log.w(TAG,
      // "CameraManager PictureCallback.  About to call native code image h/w is "
      // + h + "/" + w);
      // // opencv.setSourceImage(pixels, w, h);
      // byte[] calculatedData = opencv.findContours( w, h);

      try {
        // Write to SD Card
        outStream = new FileOutputStream(
        String.format("/sdcard/ketai_data/%d.jpg", 
        System.currentTimeMillis()));

        outStream.write(data);
        outStream.close();
      } 
      catch (FileNotFoundException e) {
        e.printStackTrace();
      } 
      catch (IOException e) {
        e.printStackTrace();
      } 
      finally {
      }
    }
  };

  public void stop() {
    PApplet.println("Stopping Camera...");
    if (camera != null && isStarted) {
      isStarted = false;
      camera.stopPreview();
      camera.setPreviewCallback(null);
      camera.release();
      camera = null;
    }
    // runner = null; // unwind the thread
  }


  @Override
    static public void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {
    // Pulled directly from:
    // http://ketai.googlecode.com/svn/trunk/ketai/src/edu/uic/ketai/inputService/KetaiCamera.java
    final int frameSize = width * height;

    // dans YUV Y correspond a la luminosité
    int u=0, v=0;
    for (int j = 0, yp = 0; j < height; j++) { 
      // int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;      
      for (int i = 0; i < width; i++, yp++) {


        int y = (0xff & ((int) yuv420sp[yp])) - 16;
        v = (0xff & yuv420sp[yp]) - 128;
        u = (0xff & yuv420sp[yp]) - 128;

        //        y -= 16;
        //        u -= 128;
        //        v -= 128;

        if (y < 0) y = 0;


        // YCbCr --> RGB
        //        int y1000 = 2000*y;
        //        int r = (y + 1140*v);
        //        int g = (y - 394*u - 518*v);
        //        int b = (y + 2032*u);

        //ou

        // YCrCb --> RGB
        //        int r = 1164*(y-16) + 1596*(v-128);
        //        int g = 1164*(y-16) - 813*(v-128) - 392*(u-128);
        //        int b = 1164*(y-16) + 2017*(u-128);

        // coversion en noir et blanc
        /*int r = 1164*(y-16);
         int g = 1164*(y-16);
         int b = 1164*(y-16);*/

        // conversion DIY
        int _Y = 1164*y;
        int r = _Y + 1596*v;
        int g = _Y - 812*v - 392*u;
        int b = _Y + 2017*u;


        //calibration
        if (r < 0)
          r = 0;
        else if (r > 262143)
          r = 262143;
        if (g < 0)
          g = 0;
        else if (g > 262143)
          g = 262143;
        if (b < 0)
          b = 0;
        else if (b > 262143)
          b = 262143;


        if(PRINT_RGB) affichergb(r, g, b);

        //definition des couleurs avec les operateur shift (acces binaire)
        rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
      }
    }
  }
  // permet d'afficher les valeurs de rgb
  static public void affichergb(int r, int g, int b) {
    PApplet.println("r = "+((r << 6) & 0xff0000)+"  g = "+((g >> 2) & 0xff00)+"  b ="+ ((b >> 10) & 0xff));
    PRINT_RGB = false;
  }
  
  public void startService() {
    if (!isStarted || camera == null)
      start();
  }

  public int getStatus() {
    if (isStarted)
      return IKetaiInputService.STATE_STARTED;
    else
      return IKetaiInputService.STATE_STOPPED;
  }

  public void stopService() {
    stop();
  }

  public String getServiceDescription() {
    return "Android camera access.";
  }

  public void registerAnalyzer(IKetaiAnalyzer _analyzer) {
    if (listeners.contains(_analyzer))
      return;
    PApplet.println("KetaiCamera Registering this analyzer: "
      + _analyzer.getClass());
    listeners.add(_analyzer);
  }

  public void removeAnalyzer(IKetaiAnalyzer _analyzer) {
    listeners.remove(_analyzer);
  }

  public void broadcastData(Object data) {
    for (IKetaiAnalyzer analyzer : listeners) {
      analyzer.analyzeData(data);
    }
  }
}

