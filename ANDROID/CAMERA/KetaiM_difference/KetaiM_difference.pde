
/*

 Analyzing movement in the preview camera.
 This sketch come with a Ketai.jar modified, to use with Samsung Galaxy Spica i5700.
 You need to activate this permissions:
 
 CAMERA
 
 */

import edu.uic.ketai.inputService.KetaiCamera;

KetaiCamera cam;
// image displays on the screen, cam isonly here to give pixels to algorithms
PImage temp;
int numPixels;
int[] previousFrame;

/************************************************************************
 
 --------------------------------  SETUP ---------------------------------
 
 *************************************************************************/
void setup() {
  //reduce frameRate because Spica is a cheap CPU 800MHz  
  frameRate(15);
  orientation(LANDSCAPE);

  cam = new KetaiCamera(this, 160, 90, 15);
  temp = createImage(cam.frameWidth, cam.frameHeight, RGB);
  numPixels = cam.frameWidth * cam.frameHeight;
  // for containing pixels from the previous frame
  previousFrame = new int[numPixels];

}
/************************************************************************
 
 --------------------------------  DRAW ---------------------------------
 
 *************************************************************************/
void draw() {
  //
  // In this version we analyze only the red value, because,this ketaiCamera version
  // is only Black & White. 
  //
  temp.loadPixels();
  int movementSum = 0; // Amount of movement in the frame
  for (int i = 0; i < numPixels; i++) { // For each pixel in the video frame...
    color currColor = cam.pixels[i];
    color prevColor = previousFrame[i];
    // Extract the red, green, and blue components from current pixel
    int currR = (currColor >> 16) & 0xFF; // Like red(), but faster
    //int currG = (currColor >> 8) & 0xFF;
    //int currB = currColor & 0xFF;
    // Extract red, green, and blue components from previous pixel
    int prevR = (prevColor >> 16) & 0xFF;
    //int prevG = (prevColor >> 8) & 0xFF;
    //int prevB = prevColor & 0xFF;
    // Compute the difference of the red, green, and blue values
    int diffR = abs(currR - prevR);
    //int diffG = abs(currG - prevG);
    //int diffB = abs(currB - prevB);
    // Add these differences to the running tally
    movementSum += diffR;// + diffG + diffB;
    // Render the difference image to the screen
    temp.pixels[i] = color(diffR);//, diffG, diffB);
    // The following line is much faster, but more confusing to read
    //pixels[i] = 0xff000000 | (diffR << 16) | (diffG << 8) | diffB;
    // Save the current color into the 'previous' buffer
    previousFrame[i] = currColor;
  }
  // To prevent flicker from frames that are all black (no movement),
  // only update the screen if the image has changed.
  if (movementSum > 0) {

    temp.updatePixels();
    image(temp, 0, 0, screenWidth, screenHeight);
    println(movementSum); // Print the total amount of movement to the console
  }
}

/************************************************************************

--------------------------------  EVENTS ---------------------------------

*************************************************************************/

void onCameraPreviewEvent()
{
  cam.read();
}

void exit() {
  cam.stop();
}

// start/stop camera preview by tapping the screen
void mousePressed()
{
  if (cam.isStarted())
  {
    cam.stop();
  }
  else
    cam.start();
}

