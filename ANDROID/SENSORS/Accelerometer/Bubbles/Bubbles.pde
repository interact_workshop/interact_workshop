import edu.uic.ketai.inputService.*;
import android.content.pm.ActivityInfo;
KetaiSensorManager sensorManager;
PVector  accelerometer;

int numBalls = 12;
float spring = 0.05;
float gravity = 0.03;
float friction = -0.5;
Ball[] balls = new Ball[numBalls];

void setup()
{
  size(screenWidth, screenHeight);
  orientation(PORTRAIT);
  sensorManager = new KetaiSensorManager(this);
  sensorManager.start();
  smooth();
  noStroke();
  accelerometer = new PVector();
  
  for (int i = 0; i < numBalls; i++) {
    balls[i] = new Ball(random(width), random(height), random(20, 40), i, balls);
  }
}

void draw()
{
  background(0);
//
//   text("Accelerometer data:" 
//  + nf(accelerometer.x, 2, 2) + "/" 
//  + nf(accelerometer.y, 2, 2) + "/" 
//  + nf(accelerometer.z, 2, 2), 5, 20);
 
  for (int i = 0; i < numBalls; i++) {
    balls[i].collide();
    balls[i].move();
    balls[i].display();
  } 
  
}


void onAccelerometerSensorEvent(long time, int accuracy, float x, float y, float z)
{
  accelerometer.set(x,y,z);
}

public void mousePressed() { 
  if(sensorManager.isStarted())
    sensorManager.stop(); 
  else
    sensorManager.start(); 
  println("SensorManager isStarted: " + sensorManager.isStarted());
}

