/*

 This sketch come with a Ketai.jar modified, to use with Samsung Galaxy Spica i5700.
 
 
 */

import edu.uic.ketai.inputService.*;
import android.content.pm.ActivityInfo;

KetaiSensorManager sensorManager;
PVector magneticField;
String[] fontList;
PFont androidFont;


void setup() {
  setRequestedOrientation(android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

  sensorManager = new KetaiSensorManager(this);
  sensorManager.start();

  magneticField = new PVector();

  boolean MagneticField =   sensorManager.isMagenticFieldSensorAvailable();
  println(MagneticField);

  fontList = PFont.list();
  androidFont = createFont(fontList[4], 20, true);
  textFont(androidFont);
}

void draw() {
  background(200);
  text("MagneticField data:\n" +
    nfp(magneticField.x, 3, 2) +
    "\n" + nfp(magneticField.y, 3, 2) +
    "\n" + nfp(magneticField.z, 3, 2), 30, 250);
}

void onMagneticFieldSensorEvent(long time, int accuracy, float x, float y, float z) {
  //All values are in micro-Tesla (uT) and measure the ambient magnetic field in the X, Y and Z axis. 
  magneticField.set(x, y, z);
}

