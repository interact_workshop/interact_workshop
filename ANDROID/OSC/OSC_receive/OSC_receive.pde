
/**
 * oscP5sendreceive by andreas schlegel
 * example shows how to send and receive osc messages.
 * oscP5 website at http://www.sojamo.de/oscP5
 */

import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;
float _X, _Y, _Z;
float smoothX, smoothY, smoothZ;
float easing = 0.05;

void setup() {
  size(400,400,P3D);
  frameRate(25);
  strokeWeight(4);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,12000);
  /*change 192.168.0.10 to your own IP address*/
  myRemoteLocation = new NetAddress("192.168.0.12",12000);
}

void draw() {
  background(246, 255, 0);
  smoothX += (_X-smoothX)*easing;
  smoothY += (_Y-smoothY)*easing;
  smoothZ += (_Z-smoothZ)*easing;
//  float x = map(_X, -10, 10, 0, HALF_PI);
//  float y = map(_Y, -10, 10, 0, HALF_PI);
//  float z = map(_Z, -10, 10, 0, HALF_PI);
  float x = map(smoothX, -10, 10, 0, HALF_PI);
  float y = map(smoothY, -10, 10, 0, HALF_PI);
  float z = map(smoothZ, -10, 10, 0, HALF_PI);
//  smoothX = (_X-smoothX)*easing;
//  smoothY = (_Y-smoothY)*easing;
//  smoothZ = (_Z-smoothZ)*easing;
  translate(width/2, height/2);
  rotateX(x);
  rotateY(y);
  rotateZ(z);
  box(100);
}

/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage theOscMessage) {

  if(theOscMessage.checkAddrPattern("/android")) 
  {
    /* print the address pattern and the typetag of the received OscMessage */
    //  print("### received an osc message.");
    //  print(" addrpattern: "+theOscMessage.addrPattern());
    //  println(" typetag: "+theOscMessage.typetag());
    _X = theOscMessage.get(0).floatValue();
    _Y = theOscMessage.get(1).floatValue();
    _Z = theOscMessage.get(2).floatValue();
  }
}

