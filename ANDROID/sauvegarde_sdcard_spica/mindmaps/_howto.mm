<map version="0.9.0" NAME="Getting started" DATE="1300108551000" FILE="howto.mm" BGCOLOR="#FDFDE1">
<node ID="ID_3842803271" TEXT="Getting started" COLOR="#000034" STYLE="NodeMasterGraphic" SELECTED="true">
<font NAME="SansSerif" SIZE="20" />
<edge COLOR="#309EFF" />
<icon BUILTIN="button_ok" />
<node ID="ID_0068772448" TEXT="User interface" POSITION="right" COLOR="#0129d6" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="20" BOLD="true" />
<edge COLOR="#0183E0" />
<icon BUILTIN="lightning" />
<node ID="ID_5742102607" TEXT="Basics " COLOR="#000034" STYLE="NodeBasicGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#01B11E" />
<node ID="ID_6641404674" TEXT="Create and delete nodes with buttons in the bottom right " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-1" />
</node>
<node ID="ID_1134477124" TEXT="Show menu by double tapping any node" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-2" />
</node>
<node ID="ID_0170362778" TEXT="Expanding nodes: tap the circle to my right! " COLOR="#000034" STYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-3" />
<icon BUILTIN="messagebox_warning" />
<node ID="ID_5725256535" TEXT="That's it! " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5707680708" TEXT="Read node notes via double tap" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-4" />
<richcontent TYPE="NOTE">
<pre>You found the node notes! Look out for the notes icon that appears when there are notes associated with a node. There are many on this map!</pre>
</richcontent>
</node>
</node>
<node ID="ID_1117018300" TEXT="Menu" COLOR="#000034" STYLE="NodeBasicGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#0183E0" />
<node ID="ID_1816505473" TEXT="Menu appear when you double tap a node" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#0183E0" />
<icon BUILTIN="info" />
</node>
<node ID="ID_5040015604" TEXT="The menu system allows you to edit the structure and presentation of maps" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#0183E0" />
</node>
</node>
<node ID="ID_3383515374" TEXT="Gestures " COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#01B11E" />
<node ID="ID_5371214404" TEXT="Toolbars too slow for you? No hardware keyboard? Try gestures! " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#01B11E" />
<arrowlink ARROW_ID="ID_5856837361" COLOR="#EBDB01" DESTINATION="ID_8132106418" STARTINCLINATION="0;-150;" ENDINCLINATION="0;-150;" STARTARROW="None" ENDARROW="Default" />
</node>
</node>
<node ID="ID_4023274046" TEXT="Drag and drop" COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#EBDD01" />
<node ID="ID_6861365727" TEXT="'Long press' on a node to grab it, then drag over another node to drop it in " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#EBDD01" />
<node ID="ID_3154812471" TEXT="The highlighted yellow node represents where the dragged node will be dropped" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#EBDD01" />
</node>
</node>
</node>
<node ID="ID_0650807268" TEXT="Zooming " COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#01C6BC" />
<node ID="ID_6653487252" TEXT="Pinch to zoom" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#01C6BC" />
</node>
<node ID="ID_2712570066" TEXT="Volume keys" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#01C6BC" />
</node>
</node>
<node ID="ID_7314453701" TEXT="Keyboard shortcuts " COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#AE49FD" />
<node ID="ID_4414873024" TEXT="If you have a hardware keyboard, you can use keyboard shortcuts " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#01B11E" />
</node>
<node ID="ID_8023477803" TEXT="All shortcuts are configurable via settings " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#01B11E" />
</node>
<node ID="ID_1123721061" TEXT="See help for more info " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#01B11E" />
</node>
<node ID="ID_1143631807" TEXT="No hardware keyboard? Try gestures instead! " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#01B11E" />
<arrowlink ARROW_ID="ID_0755312515" DESTINATION="ID_5371214404" STARTINCLINATION="147;-129;" ENDINCLINATION="12;125;" STARTARROW="None" ENDARROW="Default" />
</node>
</node>
</node>
<node ID="ID_1881524435" TEXT="Features " POSITION="left" COLOR="#0129d6" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="22" BOLD="true" />
<edge COLOR="#309EFF" />
<icon BUILTIN="bookmark" />
<node ID="ID_8566033505" TEXT="Mind Mapping " COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#01E523" />
<node ID="ID_3625650364" TEXT="Formatting" COLOR="#000034" STYLE="NodeOvalGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#0CBC01" />
<node ID="ID_8275408153" TEXT="Node notes" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#0CBC01" />
<richcontent TYPE="NOTE">
<pre>Hi</pre>
</richcontent>
</node>
<node ID="ID_5885447636" TEXT="Node styles" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#0CBC01" />
<node ID="ID_3401262853" TEXT="Different " COLOR="#000034" STYLE="NodeBasicGraphic">
<edge COLOR="#E0AD01" />
</node>
<node ID="ID_6617562427" TEXT="Shapes" COLOR="#000034" STYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="14" ITALIC="true" />
<edge COLOR="#80D8FD" />
</node>
<node ID="ID_7845732178" TEXT="Like" COLOR="#000034" STYLE="NodeOvalGraphic">
<edge COLOR="#FD7C8E" />
</node>
<node ID="ID_5764801231" TEXT="These" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#0CBC01" />
</node>
</node>
<node ID="ID_5581085736" TEXT="Icons" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#0CBC01" />
<icon BUILTIN="idea" />
<icon BUILTIN="ksmiletris" />
<icon BUILTIN="smily_bad" />
<icon BUILTIN="ts_android" />
<icon BUILTIN="pencil" />
<icon BUILTIN="ts_heart" />
<icon BUILTIN="clanbomber" />
<icon BUILTIN="button_ok" />
<icon BUILTIN="smiley-oh" />
<icon BUILTIN="password" />
<icon BUILTIN="info" />
<icon BUILTIN="help" />
<icon BUILTIN="forward" />
<icon BUILTIN="gohome" />
<icon BUILTIN="button_cancel" />
<icon BUILTIN="ts_shield" />
<icon BUILTIN="ts_money" />
<icon BUILTIN="xmag" />
<icon BUILTIN="hourglass" />
<icon BUILTIN="ts_thumb_down" />
<icon BUILTIN="calendar" />
<icon BUILTIN="full-3" />
<icon BUILTIN="full-2" />
<icon BUILTIN="full-5" />
<icon BUILTIN="full-4" />
<icon BUILTIN="bookmark" />
<icon BUILTIN="ts_thumb_up" />
<icon BUILTIN="full-1" />
<icon BUILTIN="full-0" />
<icon BUILTIN="back" />
<icon BUILTIN="full-7" />
<icon BUILTIN="full-6" />
<icon BUILTIN="full-9" />
<icon BUILTIN="full-8" />
<icon BUILTIN="up" />
<icon BUILTIN="stop-sign" />
<icon BUILTIN="yes" />
<icon BUILTIN="lightning" />
<icon BUILTIN="attach" />
<icon BUILTIN="down" />
<icon BUILTIN="messagebox_warning" />
</node>
</node>
<node ID="ID_3804706632" TEXT="Relations " COLOR="#000034" STYLE="NodeOvalGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#D6015F" />
<node ID="ID_0111821732" TEXT="Enter relation mode from the tools menu " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6200834182" TEXT="Create relations by long-pressing the first node, and then dragging your finger to the second node " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<arrowlink ARROW_ID="ID_4150237567" DESTINATION="ID_5811177431" STARTINCLINATION="379;3;" ENDINCLINATION="27;115;" STARTARROW="None" ENDARROW="Default" />
</node>
<node ID="ID_5811177431" TEXT="Example relation " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4406721504" TEXT="Hyperlinks " COLOR="#000034" STYLE="NodeOvalGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#DB9B01" />
<node ID="ID_8728788634" TEXT="Web and email links in node text click clickable" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<richcontent TYPE="NOTE">
<pre>www.thinkingspace.net support@thinkingspace.net</pre>
</richcontent>
</node>
<node ID="ID_4800700384" TEXT="Pro only... " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<icon BUILTIN="ts_android" />
<node ID="ID_5567760711" TEXT="Create hyperlinks to device files" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<node ID="ID_5348536484" TEXT="Pdfs" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
</node>
<node ID="ID_7668011184" TEXT="Images " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
</node>
<node ID="ID_8367042751" TEXT="Voice recordings " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
</node>
<node ID="ID_5662011877" TEXT="Other mindmap files " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
</node>
</node>
<node ID="ID_7176282424" TEXT="See the title screen for details on Thinking Space Pro" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
</node>
</node>
</node>
</node>
<node ID="ID_8472420448" TEXT="Advanced " COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#AB7CFD" />
<node ID="ID_8346651462" TEXT="Internet services " COLOR="#000034" STYLE="NodeOvalGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#AB7CFD" />
<node ID="ID_2037660066" TEXT="Cloud server " COLOR="#000034" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" BOLD="true" />
<edge COLOR="#AB7CFD" />
<icon BUILTIN="bookmark" />
<node ID="ID_7630128500" TEXT="Thinking Space has its own cloud server at http://thinking-space.appspot.com " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#AB7CFD" />
</node>
<node ID="ID_3585847617" TEXT="This provides a simple web portal to upload and download maps to your phone" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#AB7CFD" />
</node>
<node ID="ID_2241084538" TEXT="See help for more details " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#AB7CFD" />
</node>
</node>
<node ID="ID_4855102528" TEXT="MindMeister api support " COLOR="#000034" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" BOLD="true" />
<edge COLOR="#AB7CFD" />
<icon BUILTIN="bookmark" />
<node ID="ID_4512282546" TEXT="Only available for premium Mindmeister accounts" COLOR="#e04301" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="14" BOLD="true" />
<edge COLOR="#AB7CFD" />
</node>
<node ID="ID_6025266681" TEXT="Thinking Space supports the Mindmeister api" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#AB7CFD" />
</node>
<node ID="ID_8776206172" TEXT="This enables Thinking Space to upload and edit online Mindmeister maps" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#AB7CFD" />
</node>
</node>
</node>
</node>
<node ID="ID_8132106418" TEXT="Gestures " COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#E5A201" />
<node ID="ID_1746335785" TEXT="Great way for users without hardware keyboards to do quick mindmapping " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#E5A201" />
</node>
<node ID="ID_1463475125" TEXT="Gestures allow you to draw shapes in order to execute map commands " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#E5A201" />
<node ID="ID_6532303400" TEXT="Such as make text bold " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#E5A201" />
</node>
</node>
</node>
<node ID="ID_6866766433" TEXT="Compatibility " COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#DB9B01" />
<node ID="ID_1108176617" TEXT="Compatible with desktop mind mapping software " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<node ID="ID_5744045553" TEXT="Freemind" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<richcontent TYPE="NOTE">
<pre>http://freemind.sourceforge.net/wiki/index.php/Main_Page</pre>
</richcontent>
</node>
<node ID="ID_6321181265" TEXT="Mind Manager" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<richcontent TYPE="NOTE">
<pre>www.mindmanager.com</pre>
</richcontent>
</node>
<node ID="ID_6225405612" TEXT="Xmind via freemind import/export" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<richcontent TYPE="NOTE">
<pre>http://www.xmind.net/</pre>
</richcontent>
</node>
</node>
<node ID="ID_7326025127" TEXT="Files saved to Sdcard under mindmaps folder " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
</node>
<node ID="ID_4755826146" TEXT="Compatible with web based mind mapping software " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
<node ID="ID_7810132288" TEXT="www.mindmeister.com " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#DB9B01" />
</node>
</node>
</node>
</node>
<node ID="ID_7852564704" TEXT="Tips " POSITION="left" COLOR="#011b8d" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="20" BOLD="true" />
<edge COLOR="#047D00" />
<icon BUILTIN="ts_thumb_up" />
<node ID="ID_3700156581" TEXT="Use 'export' feature in the menu to email an image file to your pc" COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
<node ID="ID_0221776686" TEXT="Maps are saved to your sdcard under the mindmaps folder" COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
<node ID="ID_8433700672" TEXT="Use Dropbox for seamless phone to desktop syncing" COLOR="#011b8d" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
<node ID="ID_2328506510" TEXT="Download Dropbox from the android market " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<icon BUILTIN="full-1" />
<richcontent TYPE="NOTE">
<pre>market://details?id=com.dropbox.android</pre>
</richcontent>
</node>
<node ID="ID_8303774232" TEXT="Put some mind maps in your Dropbox folder " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<icon BUILTIN="full-2" />
</node>
<node ID="ID_2441342146" TEXT="Use the Dropbox app to open/edit the Mindmap files " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<icon BUILTIN="full-3" />
</node>
</node>
<node ID="ID_4134678231" TEXT="Convert between mindjet and Freemind files formats by using the 'copy' option from the long press menu on the title screen " COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
<node ID="ID_4148010784" TEXT="Expand me for tips on editing Thinking Space mind maps on your PC or Mac.. " COLOR="#011b8d" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
<node ID="ID_4433321571" TEXT="Mindmaps created with Thinking Space can be edited with the following software... " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
</node>
<node ID="ID_6508534518" TEXT="Freemind" COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<richcontent TYPE="NOTE">
<pre>http://freemind.sourceforge.net/wiki/index.php/Main_Page</pre>
</richcontent>
</node>
<node ID="ID_1477302518" TEXT="Xmind" COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<richcontent TYPE="NOTE">
<pre>http://www.xmind.org</pre>
</richcontent>
<node ID="ID_5725743866" TEXT="Use the Freemind import export option " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<icon BUILTIN="messagebox_warning" />
</node>
</node>
<node ID="ID_5141607511" TEXT="Freeplane " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<richcontent TYPE="NOTE">
<pre>http://freeplane.sourceforge.net/wiki/index.php/Main_Page</pre>
</richcontent>
</node>
<node ID="ID_3674847073" TEXT="MindManager" COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<richcontent TYPE="NOTE">
<pre>http://www.mindjet.com</pre>
</richcontent>
</node>
<node ID="ID_8760888115" TEXT="Don't forget about Dropbox for phone to desktop syncing " COLOR="#011b8d" STYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#047D00" />
<icon BUILTIN="ts_thumb_up" />
<richcontent TYPE="NOTE">
<pre>market://details?id=com.dropbox.android</pre>
</richcontent>
<node ID="ID_4786063872" TEXT="See help for how to use Dropbox with Thinking Space " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
</node>
</node>
</node>
<node ID="ID_3500731803" TEXT="Long press nodes to drag and drop" COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
<node ID="ID_2631126467" TEXT="Gestures are a big time saver for users without hardware keyboards" COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
<node ID="ID_7528378227" TEXT="Long press the add node button to add a sibling node " COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
<node ID="ID_3243380383" TEXT="Expand and collapse all nodes on a map via your devices menu key" COLOR="#011b8d" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
<node ID="ID_6707265042" TEXT="Under the 'more' section" COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
</node>
</node>
<node ID="ID_1077452565" TEXT="Search for text in nodes or notes by using your hardware quick search/find key" COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
<node ID="ID_4532206677" TEXT="URLs in node text or notes are automatically parsed for easy opening" COLOR="#011b8d" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
<node ID="ID_2748113838" TEXT="Just double tap the node, then tap the URL at the bottom to open" COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
</node>
<node ID="ID_8477008178" TEXT="Examples.. " COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
<node ID="ID_6828441868" TEXT="support@thinkingspace.net" COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
</node>
<node ID="ID_4158226635" TEXT="http://news.bbc.co.uk" COLOR="#011b8d" STYLE="NodeTextGraphic">
<edge COLOR="#047D00" />
</node>
</node>
</node>
<node ID="ID_2142150550" TEXT="Enable Multiline mode in the settings to enable link returns in node text&#10;&#10;You can then write paragraphs within node text like this. " COLOR="#011b8d" STYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#047D00" />
</node>
</node>
<node ID="ID_7678882447" TEXT="Introduction to Mindmapping" POSITION="right" COLOR="#0129d6" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="22" BOLD="true" />
<edge COLOR="#309EFF" />
<icon BUILTIN="help" />
<node ID="ID_0765367568" TEXT="What is mindmapping?" COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<edge COLOR="#BFFD58" />
<node ID="ID_3712077820" TEXT="Visual and intuitive form of note taking which mimics the way in which we think " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#BFFD58" />
</node>
<node ID="ID_0862255366" TEXT="Creativity and productivity enhancing technique " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#BFFD58" />
</node>
<node ID="ID_0855777568" TEXT="Great system for capturing ideas and insights" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#BFFD58" />
</node>
<node ID="ID_2425161888" TEXT="&#10;http://en.wikipedia.org/wiki/Mind_map&#10;" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#BFFD58" />
</node>
</node>
<node ID="ID_0648506808" TEXT="Uses" COLOR="#000034" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" BOLD="true" />
<edge COLOR="#AB7CFD" />
<icon BUILTIN="bookmark" />
<node ID="ID_7004424021" TEXT="Problem solving" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8170508075" TEXT="Planning " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5281405565" TEXT="Developing ideas" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#349FFD" />
</node>
<node ID="ID_8175430070" TEXT="Study assistance " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5307055112" TEXT="Whatever works for you! " COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2538445286" TEXT="How does it work?" COLOR="#000034" STYLE="NodeBasicGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2583208841" TEXT="Ideas are captured in a radial manner" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7802135861" TEXT="This encourages a brainstorming approach problem solving and planning" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0663728835" TEXT="How to mind map" COLOR="#0128db" STYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="20" BOLD="true" />
<edge COLOR="#309EFF" />
<icon BUILTIN="lightning" />
<node ID="ID_7505067244" TEXT="Start by placing the main theme in the center" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5286436515" TEXT="Then pace the sub-themes as branches off of the main theme" COLOR="#000034" STYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</map>
