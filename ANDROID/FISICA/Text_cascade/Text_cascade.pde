import fisica.*;
import apwidgets.*;

PWidgetContainer widgetContainer; 
PEditText textField;

String msg = "";
FWorld world;
PFont font;

void setup() {
  size(screenWidth, screenHeight);
  orientation(PORTRAIT);
  smooth();

  widgetContainer = new PWidgetContainer(this); //create new container for widgets
  textField = new PEditText(20, 20, 150, 50); //create a textfield from x- and y-pos., width and height
  widgetContainer.addWidget(textField); //place textField in container

  Fisica.init(this);

  String[] fonts = PFont.list();
  font = createFont(fonts[0], 16, true);
  textFont(font);

  world = new FWorld();
  world.setEdges(this, color(120));
  world.remove(world.top);
  world.setGravity(0, 500);

  Texto t = new Texto("Type and ENTER");
  t.setPosition(width/2, height/2);
  t.setRotation(random(-1, 1));
  t.setFill(255);
  t.setNoStroke();
  t.setRestitution(0.75);
  world.add(t);
}

void draw() {
  background(120);

  world.step();
  world.draw(this);
}

void mousePressed() {
//  if (key == CODED && keyCode == DOWN) {
    if (!msg.equals("")) {
      Texto t = new Texto(msg);
      t.setPosition(width/2, height/2);
      t.setRotation(random(-1, 1));
      t.setFill(255);
      t.setNoStroke();
      t.setRestitution(0.65);
      world.add(t);
      msg = "";
    }
//  }  
  else {
    msg += textField.getText();
    //msg+= key;
  }
}

