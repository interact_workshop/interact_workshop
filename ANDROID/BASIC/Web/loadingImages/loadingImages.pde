/**
 * Loading Images. 
 * 
 * Loading a recent image from the US National Weather Service. 
 * Notice the date in the upper left corner of the image. 
 * Processing applications can only load images from the network
 * while running in the Processing environment. This example will 
 * not run in a web broswer and will only work when the computer
 * is connected to the Internet. 
 * permission needed :
 * INTERNET
 */
 
size(screenWidth, screenHeight);
PImage img1;
imageMode(CENTER);
img1 = loadImage("http://www.processing.org/img/shirts/beauty-2.jpg");
image(img1, width/2, height/2);

