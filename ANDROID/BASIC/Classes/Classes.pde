/**
 * Reach 1. 
 * Based on code from Keith Peters (www.bit-101.com)
 * 
 * The arm follows the position of the mouse by
 * calculating the angles with atan2(). 
 */
int nb = 23;
Foo foo [] = new Foo[nb];

void setup() {
  size(screenWidth, screenHeight, A2D);
  orientation(LANDSCAPE);

  smooth(); 
  strokeWeight(10);
  stroke(0, 100);

  for (int i=0; i<nb; i++) {
    foo[i] = new Foo();
  }
}

void draw() { 
  background(226);
  for (int i=0; i<nb; i++) {
    foo[i].update();
  }
}



class Foo {
  float x, y, x2, y2, segLength;

  Foo() {
    segLength = random(10, 30);
    x = random(screenWidth-segLength);
    y = random(screenHeight-segLength);
    x2 = random(screenWidth-segLength);
    y2 = random(screenWidth-segLength);
  }

  void update() {
    float dx = motionX - x;
    float dy = motionY - y;
    float angle1 = atan2(dy, dx);  

    float tx = motionX - cos(angle1) * segLength;
    float ty = motionY - sin(angle1) * segLength;
    dx = tx - x2;
    dy = ty - y2;
    float angle2 = atan2(dy, dx);  
    x = x2 + cos(angle2) * segLength;
    y = y2 + sin(angle2) * segLength;

    segment(x, y, angle1); 
    segment(x2, y2, angle2);

  }

  void segment(float x, float y, float a) {
    pushMatrix();
    translate(x, y);
    rotate(a);
    line(0, 0, segLength, 0);
    popMatrix();
  }
}

