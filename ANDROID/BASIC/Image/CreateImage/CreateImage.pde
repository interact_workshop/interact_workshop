/**
 * Create Image. 
 * 
 * The createImage() function provides a fresh buffer of pixels to play with.
 * This example creates an image gradient.
 */

PImage img;

void setup() 
{
  size(screenWidth, screenHeight); 
  orientation(LANDSCAPE); 
  imageMode(CENTER);
  img = createImage(screenWidth/2, screenHeight/2, ARGB);
  for (int i=0; i < img.pixels.length; i++) {
    img.pixels[i] = color(0, 90, 102, i%img.width);
  }
}

void draw() 
{
  background(204);
  image(img, screenWidth/2, screenHeight/2);
  image(img, mouseX, mouseY);
}

