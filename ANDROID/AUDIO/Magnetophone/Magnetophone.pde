/*

 Magnetophone, record a sound on the sdcard and play it.
 You need this permissions:
 RECORD_SOUND
 WRITE_EXTERNAL_STORAGE
 
 
 */

import android.media.MediaRecorder;
import android.media.MediaPlayer;
import android.os.Environment;
import android.content.Context;

import java.io.IOException;

RecordButton mRecordButton = null;
MediaRecorder mRecorder = null;
PlayButton   mPlayButton = null;
MediaPlayer   mPlayer = null;

// le chemin d'acces  mon futur fichier
String mFileName = null;

String[] fontList;
PFont androidFont;    
int text_size = 48;
boolean locked = false;
color currentcolor;
int sw, sh;

//---------------------------------------------------------------------------------------
//
//                                        SETUP
//
//---------------------------------------------------------------------------------------

void setup() {
  size(screenWidth, screenHeight, A2D);
  sw = screenWidth;
  sh = screenHeight;

  orientation(PORTRAIT);
  background(0);
  smooth();

  fontList = PFont.list();
  androidFont = createFont(fontList[0], text_size, true);
  textFont(androidFont);

  color buttoncolor = color(204);
  color highlight = color(255, 53, 0);
  ellipseMode(CENTER);

  // on declare les boutons pour l'enregistrement et la lecture
  mRecordButton = new RecordButton(sw/4, 3*(sh/4), 100, buttoncolor, highlight);
  mPlayButton = new PlayButton(3*(sw/4), 3*(sh/4), 100, buttoncolor, highlight);

  // on recupere l'emplacement de la carte SD 
  mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
  mFileName += "/audiorecordtest.3gp";
}

//-------------------------------------------------------------------------------------
//
//                                        DRAW
//
//-------------------------------------------------------------------------------------

void draw() {

  textSize(21);

  mRecordButton.recOn();
  mPlayButton.playOn();
  mRecordButton.display();
  mPlayButton.display();
}

//---------------------------------------------------------------------------------
//
//                                      EXIT  
//
//--------------------------------------------------------------------------------

void onPause() {
  super.onPause();
  if (mRecorder != null) {
    mRecorder.release();
    mRecorder = null;
  }

  if (mPlayer != null) {
    mPlayer.release();
    mPlayer = null;
  }
}
//-------------------------------------------------------------------------------
//
//                               PREPARATION LECTURE & ENREGISTREMENT
//
//--------------------------------------------------------------------------------

void onRecord(boolean start) {
  if (start) {

    startRecording();
  } 
  else {
    stopRecording();
  }
}

void onPlay(boolean start) {
  if (start) {
    startPlaying();
  } 
  else {
    stopPlaying();
  }
}

//-------------------------------------------------------------------------------
//
//                                      LECTURE
//
//------------------------------------------------------------------------------

void startPlaying() {
  background(0);
  text("lecture demarree", 20, 20);
  mPlayer = new MediaPlayer();
  try {
    mPlayer.setDataSource(mFileName);
    mPlayer.prepare();
    mPlayer.start();
  } 
  catch (IOException e) {
    println("prepare() failed");
  }
}

void stopPlaying() {
  background(0);
  text("lecture arretee", 20, 20);
  mPlayer.release();
  mPlayer = null;
}

//-------------------------------------------------------------------------------------
//
//                                    ENREGISTREMENT
//
//-------------------------------------------------------------------------------------

void startRecording() {
  background(0);
  text("enregistrement demarre", 20, 20);
  mRecorder = new MediaRecorder();
  mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
  mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
  mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
  mRecorder.setOutputFile(mFileName);

  try {
    mRecorder.prepare();
  } 
  catch (IOException e) {
    println("prepare() failed");
  }

  mRecorder.start();
}

void stopRecording() {
  background(0);
  text("enregistrement termine", 20, 20);
  mRecorder.stop();
  mRecorder.release();
  mRecorder = null;
}

