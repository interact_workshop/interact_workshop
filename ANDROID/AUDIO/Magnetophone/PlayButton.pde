class PlayButton extends CircleButton {
  boolean mStartPlaying = true;

  PlayButton(int ix, int iy, int isize, color icolor, color ihighlight) {
    super(ix, iy, isize, icolor, ihighlight);
  }

  boolean over() 
  {
    if ( overCircle(x, y, size) ) {
      over = true;
      return true;
    } 
    else {
      over = false;
      return false;
    }
  }

  void playOn() {
    if (over() && mousePressed) {
      fill(255);
      onPlay(mStartPlaying);
      mStartPlaying = !mStartPlaying;
    }
    if (mStartPlaying) {
      currentcolor = basecolor;
       } 
    else {
      currentcolor = highlightcolor;
       }
  }


void display() {
  super.display();
  fill(0);
  textSize(text_size);
  String s = "P";
  float textW = textWidth(s);
  float textH = textAscent() + textDescent();
  text(s, x-textW/4, y+textH/4);
}
}
