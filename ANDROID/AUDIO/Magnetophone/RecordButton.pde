class RecordButton extends CircleButton {
  boolean mStartRecording = true;

  RecordButton(int ix, int iy, int isize, color icolor, color ihighlight) {
    super(ix, iy, isize, icolor, ihighlight);
  }

  boolean over() 
  {
    if ( overCircle(x, y, size) ) {
      over = true;
      return true;
    } 
    else {
      over = false;
      return false;
    }
  }

  void recOn() {
    if (over() && mousePressed) {
      fill(255);
      onRecord(mStartRecording);
      mStartRecording = !mStartRecording;
    }
    if (mStartRecording) {
      currentcolor = basecolor;
    } 
    else {
      currentcolor = highlightcolor;
    }
  }


  void display() {
    super.display();
    fill(0);
    textSize(text_size);
    String s = "R";
    float textW = textWidth(s);
    float textH = textAscent() + textDescent();
    text(s, x-textW/4, y+textH/4);
    //println("mStartRecording : "+mStartRecording+" overRec : " +over+" locked : "+locked);
  }
}

